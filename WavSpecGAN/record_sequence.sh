#! /bin/sh

for seq_no in `seq 5 14`
do
    fname=sample_${seq_no}.wav
      sox -b 32 -e unsigned-integer -r 16k -c 1 -d --clobber --buffer $((16000*2*3)) /home/josh/icr2022project/WavSpecGAN/samples/${fname} trim 0 2
    echo "hit ENTER to continue"
    read junk
done
