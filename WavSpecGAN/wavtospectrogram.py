#! /usr/bin/env python3

import os
import math
import time
import glob
import shutil
from PIL import Image

import numpy as np
import scipy.io.wavfile

# import tensorflow as tf

FFT_LENGTH = 1024
WINDOW_LENGTH = 512
WINDOW_STEP = int(WINDOW_LENGTH / 2)
magnitudeMin = float("inf")
magnitudeMax = float("-inf")
phaseMin = float("inf")
phaseMax = float("-inf")


def main():
    print(os.listdir('samples'))

    # first load a file - by default we use samples/cathedral.wav
    wav_list = glob.glob('samples/*.wav')

    for fname in wav_list:
        rate, signal = load_channel_from_file(fname)
        scipy.io.wavfile.write("before_" + os.path.basename(fname), rate, signal)
        # make a spectrogram
        img = generateSpectrogramForWave(signal)
        spec_fname = 'spec_before_' + os.path.basename(fname)[:-4] + '.png'
        img.save(spec_fname, 'PNG')
        # try to convert spectrogram to .png file
        rate, recovered_signal = recoverSignalFromSpectrogram(spec_fname, rate)

        img = generateSpectrogramForWave(recovered_signal)
        img.save('spec_recovered_' + os.path.basename(fname)[:-4] + '.png', "PNG")


def load_channel_from_file(fname):
    """Load the left channel from a file.  Handle both mono and stereo.
    Returns a tuple of (rate, channel_data)"""
    print(fname)
    rate, audData = scipy.io.wavfile.read(fname)
    if len(audData.shape) == 1:       # single channel
        print('# single_channel')
        channel1 = audData       # only channel
        print(type(audData), type(channel1))
    else:                        # more than one channel
        print('# stereo_picking_left')
        channel1 = audData.T[0]  # left channel
    print(f'# rate: {rate},   samples: {len(channel1)}')

    # for now we don't trim it since we are using small samples
    # # signal_fragment = channel1[30*rate:35*rate]
    # signal_fragment = channel1[:]
    return rate, channel1


def amplifyMagnitudeByLog(d):
    return 188.301 * math.log10(d + 1)


def weakenAmplifiedMagnitude(d):
    return math.pow(10, d / 188.301) - 1


def generateLinearScale(magnitudePixels, phasePixels,
                        magnitudeMin, magnitudeMax, phaseMin, phaseMax):
    height = magnitudePixels.shape[0]
    width = magnitudePixels.shape[1]
    magnitudeRange = magnitudeMax - magnitudeMin
    phaseRange = phaseMax - phaseMin
    rgbArray = np.zeros((height, width, 3), 'uint8')

    for w in range(width):
        for h in range(height):
            magnitudePixels[h, w] = (magnitudePixels[h, w] - magnitudeMin) / (magnitudeRange) * 255 * 2
            magnitudePixels[h, w] = amplifyMagnitudeByLog(magnitudePixels[h, w])
            phasePixels[h, w] = (phasePixels[h, w] - phaseMin) / (phaseRange) * 255
            red = 255 if magnitudePixels[h, w] > 255 else magnitudePixels[h, w]
            green = (magnitudePixels[h, w] - 255) if magnitudePixels[h, w] > 255 else 0
            blue = phasePixels[h, w]
            rgbArray[h, w, 0] = int(red)
            rgbArray[h, w, 1] = int(green)
            rgbArray[h, w, 2] = int(blue)
    return rgbArray


def recoverLinearScale(rgbArray, magnitudeMin, magnitudeMax,
                       phaseMin, phaseMax):
    width = rgbArray.shape[1]
    height = rgbArray.shape[0]
    magnitudeVals = rgbArray[:, :, 0].astype(float) + rgbArray[:, :, 1].astype(float)
    phaseVals = rgbArray[:, :, 2].astype(float)
    phaseRange = phaseMax - phaseMin
    magnitudeRange = magnitudeMax - magnitudeMin
    for w in range(width):
        for h in range(height):
            phaseVals[h, w] = (phaseVals[h, w] / 255 * phaseRange) + phaseMin
            magnitudeVals[h, w] = weakenAmplifiedMagnitude(magnitudeVals[h, w])
            magnitudeVals[h, w] = (magnitudeVals[h, w] / (255 * 2) * magnitudeRange) + magnitudeMin
    return magnitudeVals, phaseVals


def generateSpectrogramForWave(signal):
    print('signal:', signal)
    start_time = time.time()
    global magnitudeMin
    global magnitudeMax
    global phaseMin
    global phaseMax
    buffer = np.zeros(int(signal.size + WINDOW_STEP - (signal.size % WINDOW_STEP)))
    buffer[0:len(signal)] = signal
    height = int(FFT_LENGTH / 2 + 1)
    width = int(len(buffer) / (WINDOW_STEP) - 1)
    magnitudePixels = np.zeros((height, width))
    phasePixels = np.zeros((height, width))

    for w in range(width):
        buff = np.zeros(FFT_LENGTH)
        stepBuff = buffer[w*WINDOW_STEP:w*WINDOW_STEP + WINDOW_LENGTH]
        # apply hanning window
        stepBuff = stepBuff * np.hanning(WINDOW_LENGTH)
        buff[0:len(stepBuff)] = stepBuff
        #buff now contains windowed signal with step length and padded with zeroes to the end
        fft = np.fft.rfft(buff)
        for h in range(len(fft)):
            #print(buff.shape)
            #return
            magnitude = math.sqrt(fft[h].real**2 + fft[h].imag**2)
            if magnitude > magnitudeMax:
                magnitudeMax = magnitude
            if magnitude < magnitudeMin:
                magnitudeMin = magnitude

            phase = math.atan2(fft[h].imag, fft[h].real)
            if phase > phaseMax:
                phaseMax = phase
            if phase < phaseMin:
                phaseMin = phase
            magnitudePixels[height-h-1, w] = magnitude
            phasePixels[height-h-1, w] = phase
    rgbArray = generateLinearScale(magnitudePixels, phasePixels,
                                  magnitudeMin, magnitudeMax, phaseMin, phaseMax)
    elapsed_time = time.time() - start_time
    print('%.2f' % elapsed_time, 's', sep='')
    # print(rgbArray)
    img = Image.fromarray(rgbArray)
    return img


def recoverSignalFromSpectrogram(filePath, rate):
    print('# recovering_from_path:', filePath)
    img = Image.open(filePath)
    data = np.array(img, dtype='uint8')
    width = data.shape[1]
    height = data.shape[0]
    print(img, data, width, height)

    magnitudeVals, phaseVals \
        = recoverLinearScale(data, magnitudeMin, magnitudeMax, phaseMin, phaseMax)

    recovered = np.zeros(WINDOW_LENGTH * width // 2 + WINDOW_STEP, dtype=np.int32)
    for w in range(width):
        toInverse = np.zeros(height, dtype=np.complex_)
        for h in range(height):
            magnitude = magnitudeVals[height - h - 1, w]
            phase = phaseVals[height - h - 1, w]
            toInverse[h] = magnitude * math.cos(phase) + (1j * magnitude * math.sin(phase))
        signal = np.fft.irfft(toInverse)
        recovered[w * WINDOW_STEP:w * WINDOW_STEP + WINDOW_LENGTH] += signal[:WINDOW_LENGTH].astype(np.int32)
    out_wav_fname = 'recovered_' + os.path.basename(filePath) + '.wav'
    scipy.io.wavfile.write(out_wav_fname, rate, recovered)
    return rate, recovered


if __name__ == '__main__':
    main()


src = '.'
for a in os.listdir(src):
    if a.startswith('spec_recovered'):
        srcpath = os.path.join(src, a)
        shutil.move(srcpath, 'recoveredspec')

for a in os.listdir(src):
    if a.startswith('spec_before'):
        srcpath = os.path.join(src, a)
        shutil.move(srcpath, 'beforespec')

for a in os.listdir(src):
    if a.startswith('before_'):
        srcpath = os.path.join(src, a)
        shutil.move(srcpath, 'beforewav')

for a in os.listdir(src):
    if a.startswith('recovered_'):
        srcpath = os.path.join(src, a)
        shutil.move(srcpath, 'recoveredwav')
