# Spectrograms Applied to Machine Learning

This project examines Fourier Transform applied to audio analysis, spectrograms, and machine learning. By using Fourier Transform to turn audio into a spectrogram image and back, a new method of AI audio training can be achieved. Furthermore, a new technique of audio and waveform manipulation can be performed with this method of transformation.

## Installation

```
git clone https://codeberg.org/waltman.joshua/icr2022project.git
```

## Usage

Make sure that the folders where the outputs are stored are empty before you run the script, then you're good to run the script. 
```
python3 wavetospectrogram.py
```

## Recore Sequence
You can use the record_sequence.sh to clips as many recordings as you wish. To change the number of recordings the script will take, change line 3 in `seq X XX`

```
for seq_no in `seq 0 99`
```

This will record 100 clips. 
